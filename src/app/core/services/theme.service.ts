import { Injectable } from '@angular/core';

interface Config {
  theme: string;
  currency?: number;
  language?: string;
}

@Injectable({ providedIn: 'root' })
export class ThemeService {
  value = 'dark';

  constructor() {
    const config: Config = JSON.parse(localStorage.getItem('config'));

    if (config?.theme) {
      this.value = config.theme;
    }
  }

  set theme(value: string) {
    this.value = value;
    const config: Config = {  theme: value };
    localStorage.setItem('config', JSON.stringify(config));
  }

  get theme(): string {
    return this.value;
  }
}
