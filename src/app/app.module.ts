import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './core/components/navbar.component';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CoreModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'tvmaze', loadChildren: () => import('./features/tvmaze/tvmaze.module').then(m => m.TvmazeModule)},
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
      { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule)},
      { path: 'users/:id', loadChildren: () => import('./features/user-details/user-details.module').then(m => m.UserDetailsModule)},
      { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule)},
      { path: 'settings', loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule)},
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
      { path: '**', redirectTo: 'home'}
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
