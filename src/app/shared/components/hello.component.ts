import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <div [style.color]="color">
      <i [class]="iconCls"></i>
      hello {{label}}
    </div>
  `,
})
export class HelloComponent implements OnInit {
  @Input() label = 'Guest';
  @Input() color = 'purple';
  @Input() iconCls = 'fa fa-user';

  constructor() { }

  ngOnInit(): void {
  }

}
