import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    
    <app-card title="THEME MANAGER">
      <button
        [ngClass]="{
          'bg-info': themeService.value === 'dark'
        }"
        (click)="themeService.theme = 'dark'">dark</button>
      
      <button
        [ngClass]="{
          'bg-info': themeService.theme === 'light' 
        }"
        (click)="themeService.theme = 'light'">light</button>
    </app-card>
  `,
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) { }

  ngOnInit(): void {
  }

}
