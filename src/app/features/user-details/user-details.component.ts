import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';

@Component({
  selector: 'app-users-details',
  template: `
    
    <div class="alert alert-danger" *ngIf="error">
      utente non esiste
    </div>
    
    <div *ngIf="data">
      <h1>{{data.name}}</h1>
      <div>{{data.age}} years old</div>
      <img
        width="100%"
        style="max-width: 300px"
        [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + data.city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
    </div>
  `,
})
export class UserDetailsComponent implements OnInit {
  data: User;
  error: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {
    const id = activatedRoute.snapshot.params.id;
    http.get<User>(`http://localhost:3000/users/${id}`)
      .subscribe(
        res => this.data = res,
        err => this.error = true
      );
  }

  ngOnInit(): void {
  }

}
