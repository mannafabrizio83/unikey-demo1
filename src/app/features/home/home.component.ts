import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `

  <div class="container">

      <app-tabbar 
        [items]="countries"
        [active]="selectedCountry"
        (tabClick)="selectCountryHandler($event)"
      ></app-tabbar>
    
      <app-card 
        *ngIf="selectedCountry"
        [title]="selectedCountry?.label" 
        icon="fa fa-link"
        (iconClick)="openURL(selectedCountry.label)"
      >
        <img width="100%" [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + selectedCountry?.label + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
        {{selectedCountry?.description}}
      </app-card>
      
      <app-card 
        title="BUTTON WIDGET"
        icon="fa fa-linkedin"
        (iconClick)="showAlert()"
      >
        <button (click)="selectedCountry = this.countries[1]">CLICK ME</button>
        <button (click)="showAlert()">CLICK ME</button>
        <button (click)="showAlert()">CLICK ME</button>
      </app-card>
  </div>
  `
})
export class HomeComponent {

  countries: Country[] = [
    { id: 1, label: 'italy', description: 'bla bla 1'},
    { id: 2, label: 'China', description: 'bla bla 2'},
    { id: 3, label: 'Germany', description: 'bla bla 3'},
  ];
  selectedCountry: Country = this.countries[0];

  showAlert(): void {
    window.alert('ciao')
  }
  openURL(city: string): void {
    window.open('https://it.wikipedia.org/wiki/' + city)
  }

  selectCountryHandler(country: Country): void {
    this.selectedCountry = country;
  }
}


interface Country {
  id: number;
  label: string;
  description: string;
}
