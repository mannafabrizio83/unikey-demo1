import { Component } from '@angular/core';
import { Series } from '../../model/series';

@Component({
  selector: 'app-tvmaze',
  template: `
    <app-tvmaze-search
      (search)="result = $event"
    ></app-tvmaze-search>
    
    <app-tvmaze-results
      [result]="result" 
      [selected]="selectedSeries"
      (selectMovie)="openDetails($event)"
    ></app-tvmaze-results>
    
    <app-tvmaze-modal 
      [series]="selectedSeries"
      [color]="'red'"
      (closeModal)="selectedSeries = null"
    ></app-tvmaze-modal>
    
    <button routerLink="help">Go to help</button>
  `,
})
export class TvmazeComponent {
  result: Series[];
  selectedSeries: Series;

  openDetails(series: Series): void {
    this.selectedSeries = series;
  }

}

