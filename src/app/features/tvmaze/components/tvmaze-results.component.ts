import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Series } from '../../../model/series';

@Component({
  selector: 'app-tvmaze-results',
  template: `

    <!--RESULT -->
    <div class="grid">
      <div *ngFor="let series of result" class="grid-item">
        <div class="movie " (click)="selectMovie.emit(series)">
          <!--THUMB-->

          <div [ngClass]="{'active': series.show.id === selected?.show.id}">
            <img *ngIf="series.show.image" [src]="series.show.image.medium">
            <div class="no-image" *ngIf="!series.show.image"></div>
          </div>
          <!--TITLE-->
          <div class="movie-text">{{series.show.name}}</div>

          <!--RATING-->
          <app-rating [value]="series.show.rating.average"></app-rating>
        </div>

      </div>
    </div>

  `,
  styles: [`

    .grid {
      display: flex;
      flex-wrap: wrap;
      flex-direction: row;
      justify-content: center;
    }
    .grid-item {

    }

    .movie {
      width: 100px;
      margin: 10px;
      text-align: center;
      cursor: pointer;
    }

    .movie .active {
      border: 3px solid red;
    }

    .movie img {
      width: 100%;
    }
    .movie-text {
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
    }

    .no-image {
      width: 100%;
      height: 140px;
      border: 1px solid #ccc;
      background: repeating-linear-gradient(
          -55deg,
          #222,
          #222 10px,
          #333 10px,
          #333 20px
      );
    }
  `]
})
export class TvmazeResultsComponent {
  @Input() result: Series[];
  @Input() selected: Series;
  @Output() selectMovie: EventEmitter<Series> = new EventEmitter<Series>()

}
