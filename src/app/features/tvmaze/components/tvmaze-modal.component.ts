import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Series } from '../../../model/series';

@Component({
  selector: 'app-tvmaze-modal',
  template: `
    <div class="series-modal" [ngClass]="{'open': isPanelOpened}">
      <div class="content">
        <i class="fa fa-2x fa-times close-button" (click)="closeModalHandler()"></i>
        <h1>{{series?.show.name}}</h1>
        <img [src]="series?.show.image?.original" alt="" width="100%">
        <div [innerHTML]="series?.show.summary"></div>
      </div>
    </div>
  `,
  styles: [`

    .series-modal {
      position: fixed;
      top: 0;
      bottom: 0;
      right: -300px;
      background-color: black;
      width: 300px;
      height: 100%;
      color: white;
      overflow-y: auto;
      transition: 1.4s right ease-in-out;
    }
    
    .series-modal.open {
      right: 0;
    }

    .series-modal .content {
      margin: 20px;
    }

    .close-button {
      position: fixed;
      top: 10px;
      right: 10px;

    }


  `]
})
export class TvmazeModalComponent implements OnChanges {
  @Input() series: Series;
  @Input() color: string;
  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
  isPanelOpened: boolean;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.series && changes.series.currentValue) {
      this.isPanelOpened = true;
    }
  }

  closeModalHandler(): void {
    this.isPanelOpened = false;

    setTimeout(() => {
      this.closeModal.emit();
    }, 1400);
  }
}
