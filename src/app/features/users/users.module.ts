import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { GenderIconComponent } from './components/gender-icon.component';
import { UsersListComponent } from './components/users-list.component';
import { UsersFormComponent } from './components/users-form.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    UsersComponent,
    GenderIconComponent,
    UsersListComponent,
    UsersFormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: UsersComponent }
    ])
  ]
})
export class UsersModule { }
