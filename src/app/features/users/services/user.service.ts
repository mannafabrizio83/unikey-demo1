import { Injectable } from '@angular/core';
import { User, UserAdd } from '../../../model/user';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class UserService {
  users: Partial<User>[];

  constructor(private http: HttpClient) {}

  getUsers(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe((response) => {
        this.users = response;
      });
  }

  deleteHandler(user: Partial<User>): void {
    this.http.delete(`http://localhost:3000/users/${user.id}`)
      .subscribe(() => {
        const index = this.users.findIndex(u => u.id === user.id);
        this.users.splice(index, 1);
      });
  }

  save(form: NgForm): void {
    this.http.post<UserAdd>('http://localhost:3000/users/', form.value)
      .subscribe(res => {
        this.users.push(res);
        form.reset();
      });
  }
}


