import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gender-icon',
  template: `
    <i
      class="fa"
      [ngClass]="{
            'fa-mars': value === 'M',
            'fa-venus': value === 'F'
           }"
    ></i>
  `,
})
export class GenderIconComponent {
  @Input() value: string;
}
