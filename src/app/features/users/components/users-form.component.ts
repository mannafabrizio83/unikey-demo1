import { AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-users-form',
  template: `
    <form
      #f="ngForm"
      (submit)="save.emit(f)"
      style="padding: 20px; border: 2px solid green; border-radius: 20px; margin: 10px"
      [style.borderColor]="f.invalid ? 'red' : 'green'"
    >

      <div *ngIf="inputName.errors?.required">il campo è obbligatorio</div>
      <div *ngIf="inputName.errors?.minlength">il campo è troppo corto. Necessari 3 chars</div>
      <input
        class="form-control mb-1"
        [ngClass]="{
              'is-invalid': inputName.invalid && f.dirty,
              'is-valid': inputName.valid
             }"
        type="text" ngModel name="name"
        required minlength="3"
        #inputName="ngModel"
        #inputRef
      >

      <input
        type="text"
        class="form-control mb-1"
        [ngClass]="{
              'is-invalid': inputCity.invalid && f.dirty,
              'is-valid': inputCity.valid
             }"
        ngModel
        #inputCity="ngModel"
        name="city" required >

      <select
        class="form-control mb-1"
        [ngModel] name="gender" required
      >
        <option [ngValue]="null">Select gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>

      <button
        class="btn btn-block miaClasse"
        [ngClass]="{
              'btn-success': f.valid,
              'btn-warning': f.invalid
             }"
        type="submit" [disabled]="f.invalid">Send</button>
    </form>
  `,
  styles: [
  ]
})
export class UsersFormComponent implements AfterViewInit {
  @ViewChild('inputRef') inputRef: ElementRef<HTMLInputElement>;
  @Output() save: EventEmitter<NgForm> = new EventEmitter<NgForm>();

  ngAfterViewInit(): void {
    this.inputRef.nativeElement.focus();
  }
}
